﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Shared.ViewModels;
using System;

namespace Api.Controllers
{
    [AllowAnonymous]
    [Route("")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class IndexController : BaseController
    {
        public IndexController()
        {
        }

        [Route(""), HttpGet]
        public IActionResult Swagger()
        {
            return Redirect("~/swagger");
        }

    }
}