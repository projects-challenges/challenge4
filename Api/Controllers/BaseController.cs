﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers

{
    public class BaseController : Controller
    {

        public BaseController()
        {
        }

        protected new IActionResult Response(object result = null)
        {
            return Ok(new
            {
                success = true,
                data = result
            });
        }

        protected IActionResult FilterResponse(object result = null)
        {
            if (result != null)
            {
                return Ok(new
                {
                    success = true,
                    data = result
                });
            }
            return NoContent();
        }

    }
}
