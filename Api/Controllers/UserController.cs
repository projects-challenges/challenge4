﻿using Domain.Service.Interface;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Shared.ViewModels;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [AllowAnonymous]
    [ApiVersion("1.0")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly IUserServiceAsync _userService;
        public UserController(IUserServiceAsync userService)
        {
            _userService = userService;
        }

        [HttpGet]
        //[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(Task<PagedResponse<IEnumerable<UserViewModel>>>))]
        public async Task<IActionResult> GetPagination([FromQuery] Pagination filter)
        {
            var items =  await _userService.Get(filter);
            return Response(items);
        }

        [HttpGet]
        [Route("GroupEmailToLookup")]
        public IActionResult GroupEmailToLookup()
        {
            var items = _userService.GroupEmailToLookup();
            return Response(items);
        }

        [HttpGet]
        [Route("GroupEmailToDictionary")]
        public IActionResult GroupEmailToDictionary()
        {
            var items = _userService.GroupEmailToDictionary();
            return Response(items);
        }

        [HttpGet]
        [Route("GroupEmailPureSql")]
        public IActionResult GroupEmailPureSql()
        {
            var result = _userService.GroupEmailPureSql();
            return Response(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var item = await _userService.GetOne(id);
            if (item == null)
            {
                Log.Error("GetById({id}) NOT FOUND", id);
                return NotFound();
            }

            return Response(item);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserViewModel item)
        {
            if (item == null)
                return BadRequest();

            var id = await _userService.Insert(item);
            return Created($"api/User/{id}", id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UserViewModel item)
        {
            if (item == null || item.Id != id)
                return BadRequest();

            int retVal = await _userService.Update(item);
            if (retVal == 0)
                return StatusCode(304);
            else if (retVal == -1)
                return StatusCode(412, "DbUpdateConcurrencyException");
            else
                return Accepted(item);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            int retVal = await _userService.Delete(id);

            if (retVal == 0)
                return NotFound();
            else if (retVal == -1)
                return StatusCode(412, "DbUpdateConcurrencyException");
            else
                return NoContent();
        }
    }
}