﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Sqlite;

namespace Entity.ADO
{
    public class AdoDataContext
    {
        private readonly IConfiguration _configuration;
        public AdoDataContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public SqliteConnection getConnection()
        {
            return new SqliteConnection(_configuration["ConnectionStrings:ApiDB"]);
        }
    }
}
