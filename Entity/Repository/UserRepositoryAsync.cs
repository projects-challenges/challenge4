﻿using Entity.ADO;
using Entity.Repository.Interface;
using Entity.UnitofWork;
using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Entity.Repository
{
    public class UserRepositoryAsync : RepositoryAsync<User>, IUserRepositoryAsync
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AdoDataContext _adoDataContext;

        public UserRepositoryAsync(IUnitOfWork unitOfWork, AdoDataContext adoDataContext) : base(unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _adoDataContext = adoDataContext;
        }

        public dynamic GroupEmailToLookup()
        {
            return (from user in _unitOfWork.Context.Users.ToLookup(x => x.Email.Split('@')[1])
                    select new
                    {
                        Domain = user.Key,
                        Count = user.Count()
                    }).ToList();
        }

        public dynamic GroupEmailToDictionary()
        {
            return _unitOfWork.Context.Users.ToList().GroupBy(x => x.Email.Split('@')[1]).ToDictionary(grp => grp.Key, grp => grp.Count());
        }

        public dynamic GroupEmailPureSql()
        {
            var list = new List<dynamic>();

            var query = "select SUBSTR(email, instr(email, '@') + 1, instr(email, '.') - 1) as domain, COUNT(*) AS count from Users GROUP BY domain;";

            using (SqliteConnection conn = _adoDataContext.getConnection())
            {
                if(conn.State == ConnectionState.Closed) conn.Open();

                using var cmd = new SqliteCommand(query, conn);
                //return (long)cmd.ExecuteScalar();
                using SqliteDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                    list.Add(new { Domain = rdr.GetString(0), Count = rdr.GetInt64(1) });

                return list;
            }
        }
    }
}
