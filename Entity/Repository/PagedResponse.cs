﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.Repository
{
    public class Response<T>
    {
        public Response()
        {
        }

        public Response(T data, int pageNumber, int pageSize, long totalPages, long totalRecords)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.TotalPages = totalPages;
            this.TotalRecords = totalRecords;
            this.Result = data;
        }

        public T Result { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long TotalPages { get; set; }
        public long TotalRecords { get; set; }
    }

    public class PagedResponse<T> : Response<T>
    {

        public PagedResponse(T data, int pageNumber, int pageSize, long totalPages, long totalRecords)
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.TotalPages = totalPages;
            this.TotalRecords = totalRecords;
            this.Result = data;
        }
    }
}
