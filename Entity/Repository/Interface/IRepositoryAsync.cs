﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Entity.Repository.Interface
{
    public interface IRepositoryAsync<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<Response<IEnumerable<T>>> Get(Pagination filter);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<T> GetOne(Expression<Func<T, bool>> predicate);
        Task<int> Insert(T entity);
        void Delete(T entity);
        Task<int> Delete(object id);
        Task<int> Update(object id, T entity);
        void ExecuteQuery(string query);
    }
}