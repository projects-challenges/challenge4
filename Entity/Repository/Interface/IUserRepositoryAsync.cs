﻿
namespace Entity.Repository.Interface
{
    public interface IUserRepositoryAsync : IRepositoryAsync<User>
    {
        dynamic GroupEmailToLookup();
        dynamic GroupEmailToDictionary();
        dynamic GroupEmailPureSql();
    }
}