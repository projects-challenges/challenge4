﻿using Entity.Repository.Interface;
using Entity.UnitofWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Entity.Repository
{
    public class RepositoryAsync<T> : IRepositoryAsync<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public RepositoryAsync(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _unitOfWork.Context.Set<T>().ToListAsync();
        }
        public virtual async Task<Response<IEnumerable<T>>> Get(Pagination filter)
        {
            var entities = await _unitOfWork.Context.Set<T>()
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();

            var totalRecords = await _unitOfWork.Context.Set<T>().CountAsync();
            long totalPages = Convert.ToInt64(Math.Ceiling(((double)totalRecords / (double)filter.PageSize)));
            return new Response<IEnumerable<T>>(entities, filter.PageNumber, filter.PageSize, totalPages, totalRecords);
        }

        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate)
        {
            return await _unitOfWork.Context.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<T> GetOne(Expression<Func<T, bool>> predicate)
        {
            return await _unitOfWork.Context.Set<T>().Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<int> Insert(T entity)
        {
            if (entity != null)
            {
                await _unitOfWork.Context.Set<T>().AddAsync(entity);
                return await _unitOfWork.SaveAsync();
            }
            return -1;
        }

        public async Task<int> Update(object id, T entity)
        {
            if (entity != null)
            {
                _unitOfWork.Context.Entry(entity).State = EntityState.Modified;

                return await _unitOfWork.SaveAsync();
            }
            return -1;
        }

        public async Task<int> Delete(object id)
        {
            T entity = await _unitOfWork.Context.Set<T>().FindAsync(id);
            Delete(entity);
            return await _unitOfWork.SaveAsync();
        }

        public void Delete(T entity)
        {
            if (entity != null) _unitOfWork.Context.Set<T>().Remove(entity);
        }

        public void ExecuteQuery(string query)
        {
            _unitOfWork.Context.Database.ExecuteSqlRaw(query);
        }

    }
}