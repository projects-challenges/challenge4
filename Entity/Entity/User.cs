﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Entity
{
    public class User : BaseEntity
    {
        [StringLength(100)]
        public virtual string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public virtual string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public virtual string Email { get; set; }
    }
}
