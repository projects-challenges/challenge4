﻿using System;

namespace Shared
{
    public class BaseViewModel
    {
        public int Id { get; private set; }

        public DateTime Created { get; private set; }

        public DateTime Modified { get; private set; }
    }
}
