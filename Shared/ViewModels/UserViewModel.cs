﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shared.ViewModels
{
    public class UserViewModel : BaseViewModel
    {
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }
    }
}
