﻿using AutoMapper;
using Entity;
using Shared.ViewModels;

namespace Domain.Mapping
{
    public partial class MappingProfile : Profile
    {
        /// <summary>
        /// Create automap mapping profiles
        /// </summary>
        public MappingProfile()
        {
            CreateMap<UserViewModel, User>().ReverseMap();

            //call code in partial scaffolded function
            SetAddedMappingProfile();
        }

        //to call scaffolded method
        partial void SetAddedMappingProfile();

    }





}
