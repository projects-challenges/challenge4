﻿using AutoMapper;
using Domain.Service.Interface;
using Entity;
using Entity.Repository.Interface;
using Entity.UnitofWork;
using Shared.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class UserServiceAsync : ServiceAsync<UserViewModel, User>, IUserServiceAsync
    {
        private readonly IUserRepositoryAsync _userRepositoryAsync;
        private readonly IMapper _mapper;
        public UserServiceAsync(IUserRepositoryAsync userRepositoryAsync, IMapper mapper) : base(userRepositoryAsync, mapper)
        {
            _userRepositoryAsync = userRepositoryAsync;
            _mapper = mapper;
        }

        public dynamic GroupEmailToLookup()
        {
            return _userRepositoryAsync.GroupEmailToLookup();
        }

        public dynamic GroupEmailToDictionary()
        {
            return _userRepositoryAsync.GroupEmailToDictionary();
        }

        public dynamic GroupEmailPureSql()
        {
            return _userRepositoryAsync.GroupEmailPureSql();
        }
    }

}
