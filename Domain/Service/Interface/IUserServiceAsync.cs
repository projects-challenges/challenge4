﻿using Entity;
using Shared.ViewModels;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IUserServiceAsync : IServiceAsync<UserViewModel, User>
    {
        dynamic GroupEmailToLookup();
        dynamic GroupEmailToDictionary();
        dynamic GroupEmailPureSql();
    }

}
