﻿using Entity;
using Entity.Repository;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Service.Interface
{
    public interface IServiceAsync<Tv, Te>
    {
        Task<IEnumerable<Tv>> GetAll();
        Task<PagedResponse<IEnumerable<Tv>>> Get(Pagination filter);
        Task<IEnumerable<Tv>> Find(Expression<Func<Te, bool>> predicate);
        Task<Tv> GetOne(int id);
        Task<int> Insert(Tv obj);
        Task<int> Update(Tv obj);
        Task<int> Delete(int id);
    }
}