﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Service.Interface
{
    public interface IService<Tv, Te>
    {
        IEnumerable<Tv> GetAll();
        IEnumerable<Tv> Find(Expression<Func<Te, bool>> predicate);
        Tv GetOne(int id);
        int Insert(Tv obj);
        int Update(Tv obj);
        int Delete(int id);
    }
}
