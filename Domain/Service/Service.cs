﻿using AutoMapper;
using Domain.Service.Interface;
using Entity;
using Entity.UnitofWork;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Domain.Service
{
    public class Service<Tv, Te> : IService<Tv, Te> where Tv : BaseViewModel
                                      where Te : BaseEntity
    {

        protected IUnitOfWork _unitOfWork;
        protected IMapper _mapper;
        public Service(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Service()
        {
        }

        public virtual IEnumerable<Tv> GetAll()
        {
            var entities = _unitOfWork.GetRepository<Te>().GetAll();
            return _mapper.Map<IEnumerable<Tv>>(source: entities);
        }

        public virtual IEnumerable<Tv> Find(Expression<Func<Te, bool>> predicate)
        {
            var entities = _unitOfWork.GetRepository<Te>()
                .Find(predicate: predicate);
            return _mapper.Map<IEnumerable<Tv>>(source: entities);
        }

        public virtual Tv GetOne(int id)
        {
            var entity = _unitOfWork.GetRepository<Te>().GetOne(predicate: x => x.Id == id);
            return _mapper.Map<Tv>(source: entity);
        }

        public virtual int Insert(Tv view)
        {
            var entity = _mapper.Map<Te>(source: view);
            _unitOfWork.GetRepository<Te>().Insert(entity);
            _unitOfWork.Save();
            return entity.Id;
        }

        public virtual int Update(Tv view)
        {
            _unitOfWork.GetRepository<Te>().Update(view.Id, _mapper.Map<Te>(source: view));
            return _unitOfWork.Save();
        }

        public virtual int Delete(int id)
        {
            Te entity = _unitOfWork.Context.Set<Te>().Find(id);
            _unitOfWork.GetRepository<Te>().Delete(entity);
            return _unitOfWork.Save();
        }

    }
}
