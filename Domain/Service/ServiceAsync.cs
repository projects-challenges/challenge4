﻿using AutoMapper;
using Domain.Service.Interface;
using Entity;
using Entity.Repository;
using Entity.Repository.Interface;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Service
{
    public class ServiceAsync<Tv, Te> : IServiceAsync<Tv, Te> where Tv : BaseViewModel
                                      where Te : BaseEntity
    {
        private readonly IRepositoryAsync<Te> _repositoryAsync;
        private readonly IMapper _mapper;
        public ServiceAsync(IRepositoryAsync<Te> repositoryAsync, IMapper mapper)
        {
            _repositoryAsync = repositoryAsync;
            _mapper = mapper;
        }

        public ServiceAsync()
        {
        }

        public virtual async Task<IEnumerable<Tv>> GetAll()
        {
            var entities = await _repositoryAsync.GetAll();
            return _mapper.Map<IEnumerable<Tv>>(source: entities);
        }

        public virtual async Task<PagedResponse<IEnumerable<Tv>>> Get(Pagination filter)
        {
            var response  = await _repositoryAsync.Get(filter);
            var mapEntities = _mapper.Map<IEnumerable<Tv>>(source: response.Result);

            return new PagedResponse<IEnumerable<Tv>>(mapEntities, filter.PageNumber, filter.PageSize, response.TotalPages, response.TotalRecords);
        }

        public virtual async Task<IEnumerable<Tv>> Find(Expression<Func<Te, bool>> predicate)
        {
            var items = await _repositoryAsync.Find(predicate: predicate);
            return _mapper.Map<IEnumerable<Tv>>(source: items);
        }

        public virtual async Task<Tv> GetOne(int id)
        {
            var entity = await _repositoryAsync.GetOne(predicate: x => x.Id == id);
            return _mapper.Map<Tv>(source: entity);
        }

        public virtual async Task<int> Insert(Tv view)
        {
            var entity = _mapper.Map<Te>(source: view);
            await _repositoryAsync.Insert(entity);
            return entity.Id;
        }

        public async Task<int> Update(Tv view)
        {
            return await _repositoryAsync.Update(view.Id, _mapper.Map<Te>(source: view));
        }

        public virtual async Task<int> Delete(int id)
        {
            return await _repositoryAsync.Delete(id);
        }
    }
}
